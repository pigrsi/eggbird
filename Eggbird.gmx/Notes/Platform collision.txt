Static ground:
It's checked whether moving in x would put you in the wall. If so, you're stopped.
It's checked whether moving in y would put you in the ceiling/floor. If so, you're stopped.

Moving platform:
Everything on the platform should be moved in the same direction as the platform.

If an object is on a platform moving down, it should drag them down with it, not rely on gravity:
	If the object is pulled down, the object checks collision, and THEN the platform moves, their x will be stopped, because they're put inside the platform.
	They should get pulled, then the platform, and then they check collision.
