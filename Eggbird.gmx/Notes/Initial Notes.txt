Art:
Loads of Balls (fun)
Hazards
Backgrounds - preferably ones that move a bit. Choice of background depends on which level the player is on, and will change
every 50 or so.
The outline colour of objects will change upon reaching a checkpoint.

Game:
Presentation:
The main menu will be overlaid on top of the game. When you load the game, you will be on your current level, and
the game can start just by clicking Start.

GUI: 
Current level
Suicide button
Back button will pause (obv. different on ios)

Gameplay:
You are a ball. You don't move so fast. It won't move unless you tap the screen. Tap again and it stops. (Allow option for hold to move?)
You only go in one direction. This direction depends on the level you're on. You can't jump, you have to ride platforms to elevate.
Your goal is to get to the next checkpoint without breaking the ball. That's it. The ball breaks in one hit but most levels are short.

Level design:
The ball will always be a bit nearer to the side of the screen the ball is moving away from, but closer to the centre.
There will be no slopes.
Levels are split into short things between checkpoints.
When dead, the camera will pan back to the checkpoint and ball will respawn quickly.

Hazards:
You die in one hit and go back to checkpoint.
Spikes
Tar
Lightning
Being Crushed
Various moving things that kill on touch


Interactables:
Water - float to the top. Will sink for a bit depending on fall height.
Tar - will sink slowly. Die when completely submerged. Covers ball black until death or checkpoint.
Moving platforms - Move up and down. Can crush the ball between the floor or the ceiling.
Springs - duh.
Tasty Skin - you can pick up different sprites for your ball. Your ball will change to whatever skin was picked up when you get one,
until you die. This gives a little preview of the skin, which can later be equipped permanently from the menu. Maybe some skins could have
special effects like colouring the floor or making different noises, or poisoning water. Need to decide whether player can see a ? for the pickup
or the actual skin they will get.
Trails - the same thing as skins.

Physics:
Will only need basic physics

Sound:
Sound but no music

Ad:
Interstitials after every few checkpoints reached or dying loads of times on a level. (Maybe copy ball game where
the player can choose to watch more ads to have more tries?)
Maybe pay a buck to remove ads forever.

Structure:
No packs or anything, the whole game is just a series of levels indefinitely. There should be enough levels that
nobody will reach the end (Like desert golfing but not generated). There will be a level select where you type the level you want to go to if you've beaten it already.