// From https://www.yoyogames.com/blog/66
// Initialise surface size
var base_w = 800;
var base_h = 480;
var aspect = base_w / base_h ; // get the GAME aspect ratio

// Take the lower of the two heights, so the game will be scaled up
var hh = min(base_h, display_get_height());
var ww = hh * aspect;
surface_resize(application_surface, ww, hh);
