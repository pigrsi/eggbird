///draw_rect_outline(colour)
var colour = argument[0];

var left = self.x;
var right = self.x + self.sprite_width;
var top = self.y;
var bottom = self.y + self.sprite_height;

// Top
draw_line_width_colour(left, top, right, top, 2, colour, colour);
// Bottom
draw_line_width_colour(left, bottom, right, bottom, 2, colour, colour);
// Left
draw_line_width_colour(left, top, left, bottom, 2, colour, colour);
// Right
draw_line_width_colour(right, top, right, bottom, 2, colour, colour);
