/// Movement & Collision

y_speed += player_gravity * global.delta;
if (y_speed > 25) y_speed = 25;

// Collision
if (place_meeting(x + x_speed*global.delta, y, obj_solid_ground)) {
    x_speed = 0;
}
if (place_meeting(x, y + y_speed*global.delta, obj_solid_ground)) {
    y_speed = 0;
}

x += x_speed * global.delta;
y += y_speed * global.delta;

x_speed = 0;
