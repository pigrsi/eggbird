// Should happen super early - in the initialisation room

// Screen sizes
global.game_width = 800;
global.game_height = 480;

// Declare all global variables
global.delta = 1;

global.game_is_paused = false;

// Randomly decide whether the colour's values will go up or down
//global.border_red_rising = (irandom(1) == 0);
//global.border_green_rising = (irandom(1) == 0);
//global.border_blue_rising = (irandom(1) == 0);

